<?php

use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

// seul "require" que nous écrirons, l'autoload de composer se chargera de charger tous les fichiers php
// s'ils respectent une certaine nomenclature (psr-4 voir composer.json)
// les commentaires ne sont pas permis en json, je commente donc ici
// toutes les classes qui se trouveront dans le dossier "src" devront avoir le namespace "TwigDemo"
// toutes les classes qui se trouveront dans un sous dossier de src auront le namespace "TwigDemo\[subFolderName]"
// toutes les classes qui se trouveront dans le dossier "core" devront avoir le namespace "Core"
// toutes les classes qui se trouveront dans un sous dossier de core auront le namespace "Core\[subFolderName]"
// a chaque changements dans la partie autoload
// executer la cmd composer dump-autoload
require_once 'vendor/autoload.php';

session_start();

// on spécifie au départ le dossier dans lequel se trouveront nos "vues"
$loader = new FilesystemLoader('src/view');

// on crée une instance de Twig qui sera fournie à nos controller
$twig = new Environment($loader);

define ('SITE_ROOT', realpath(dirname(__FILE__)));

define('SITE_DOMAIN',
    preg_replace('/\?.*/', "", $_SERVER["REQUEST_URI"])
);

$twig->addFunction(new TwigFunction("assets", function($path) {
    return  SITE_DOMAIN . "/assets/" . $path;
}));

$twig->addFunction(new TwigFunction("path", function(
    $method =  null,
    $controller = null,
    $parameters = []
) {
    $url = SITE_DOMAIN;
    if ($method){
        $url .= "?method=$method";
        if ($controller) {
            $url .= "&controller=$controller";
        }
        foreach ($parameters as $key => $value) {
            $url .= "&$key=$value";
        }
    }
    return $url;
}));

//on récupère ici le nom du controller qui sera instancié par défaut "Default"
$controllerName = isset($_GET["controller"]) ? $_GET["controller"] : "Default";

//on récupère ici le nom de la méthod du controller qui sera appelée par défaut "home"
$method = isset($_GET["method"]) ? $_GET["method"] : "home";

//on instancie le controller et on appelle une méthode
$controllerClassName = "TwigDemo\\controller\\" . $controllerName . "Controller";
if (class_exists($controllerClassName)){
    $controller = new $controllerClassName($twig);
    if(method_exists($controller, $method)) {
        echo $controller->$method();
    }
    else{
        echo 404;
    }
}
else{
    echo 404;
}


