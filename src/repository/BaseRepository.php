<?php


namespace TwigDemo\repository;


use PDO;

abstract class BaseRepository
{

    /**
     * @var PDO
     */
    protected $pdo;

    public function __construct()
    {
        $this->pdo = new PDO("mysql:host=localhost;port=3306;dbname=demo", "root", null);
    }
}