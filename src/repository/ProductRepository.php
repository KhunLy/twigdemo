<?php


namespace TwigDemo\repository;


use PDO;
use TwigDemo\model\Product;

class ProductRepository extends BaseRepository
{
    /**
     * @return Product[]
     */
    public function getAll() {
        $stmt = $this->pdo->prepare("SELECT * FROM Product");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS, Product::class);
    }

    /**
     * @param $id int
     * @return Product|null
     */
    public function get($id)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM product WHERE id = ?");
        $stmt->execute([
            $id
        ]);
        return $stmt->fetchObject(Product::class);
    }

    /**
     * @param Product $product
     * @return int
     */
    public function insert(Product $product) {
        $stmt = $this->pdo->prepare(
            "INSERT INTO product(id, name, description, price, discount, image) 
                VALUES (null, ?, ?, ?, ?, ?)"
        );
        $stmt->execute([
            $product->getName(),
            $product->getDescription(),
            $product->getPrice(),
            $product->getDiscount(),
            $product->getImage()
        ]);
        return $this->pdo->lastInsertId();
    }

    /**
     * @param Product $product
     */
    public function update(Product $product) {
        $stmt = $this->pdo->prepare(
            "UPDATE product SET 
                name = ?,
                description = ?,
                price = ?,
                discount = ?,
                image = ?
            WHERE id = ?"
        );
        $stmt->execute([
            $product->getName(),
            $product->getDescription(),
            $product->getPrice(),
            $product->getDiscount(),
            $product->getImage(),
            $product->getId()
        ]);
    }

    /**
     * @param $id int
     */
    public function delete($id)
    {
        $stmt = $this->pdo->prepare("DELETE FROM product WHERE id = ?");
        $stmt->execute([
            $id
        ]);
    }
}