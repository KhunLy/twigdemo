<?php
namespace TwigDemo\model;

use Core\exception\ValidationException;

class Product
{
    private $id;
    private $name;
    private $description;
    private $image;
    private $price;
    private $discount;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     * @throws ValidationException
     */
    public function setName($name)
    {
        if(!$name) {
            throw new ValidationException("This field is required");
        }
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return Product
     * @throws ValidationException
     */
    public function setImage($image)
    {
        if (!$image) {
            throw new ValidationException("This Field is required");
        }
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Product
     * @throws ValidationException
     */
    public function setPrice($price)
    {
        if(!$price){
            throw new ValidationException("This Field is requied");
        }
        if(!filter_var($price, FILTER_VALIDATE_FLOAT)){
            throw new ValidationException("Not Valid Price");
        }
        $this->price = floatval($price);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     * @return Product
     * @throws ValidationException
     */
    public function setDiscount($discount)
    {
        if(filter_var($discount ?: 0, FILTER_VALIDATE_INT) === false){
            throw new ValidationException("Not Valid Number");
        }
        if(intval($discount) > 100 || intval($discount) < 0){
            throw new ValidationException("This value must be between 0 and 100");
        }
        $this->discount = intval($discount);
        return $this;
    }



    public function getDiscountPrice() {
        return $this->price - ($this->price * ($this->discount ?: 0) / 100);
    }
}