<?php

namespace TwigDemo\controller; 

abstract class BaseController{
    private $twig; 

    public function __construct($twig){
        $this->twig = $twig; 
    }

    protected function render($view, $parameters = []){
        echo $this->twig->render($view, $parameters); 
    }

}