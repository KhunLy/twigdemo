<?php
namespace TwigDemo\controller;

use Core\controller\BaseController;
use DateTime;
use TwigDemo\model\Product;


// on hérite de la classe de base des controllers
// c'est elle qui permettra à nos controllers d'utiliser twig via sa méthode render()
class DefaultController extends BaseController
{
    public function home() 
    {
        // variable qui sera envoyée à la vue
        $variable = "Khun";
        return $this->render("Default/home.html.twig", [
            "nomDeLaVariableDansTwig" => $variable,
        ]);
    }

    public function filters() 
    {
        $chaine = "une chaine de caractères";
        $date = new DateTime('1970-01-01');
        $nombre = 3.14159;
        return $this->render("Default/filters.html.twig", [
            "chaine" => $chaine,
            "date" => $date,
            "nombre" => $nombre,
        ]);
    }

    public function loops()
    {
        return $this->render("Default/loops.html.twig", [
            'liste' => ["Pommes", "Poires", "Pêches"]
        ]);
    }

    public function model()
    {
        $p = new Product();
        $p->setName("Coca Cola")
            ->setDescription("Boisson Gazeuze au goût de Cola")
            ->setPrice(1.5)
            ->setDiscount(25)
            ->setImage("images/coca.jpg");
        return $this->render("Default/model.html.twig", [
            "product" => $p
        ]);
    }

    public function heritage() 
    {
        return $this->render("Default/heritage.html.twig");
    } 
}