<?php


namespace TwigDemo\controller;


use Core\controller\BaseController;
use Core\exception\NotFoundException;
use TwigDemo\model\Product;
use TwigDemo\repository\ProductRepository;

class ProductController extends BaseController
{
    public function index()
    {
        $repo = new ProductRepository();
        $products = $repo->getAll();
        return $this->render("Product/index.html.twig", [
            "products" => $products
        ]);
    }

    public function details() {
        $id = $this->get("id");
        $repo = new ProductRepository();
        $product = $repo->get($id);
        if(!$product){
            throw new NotFoundException();
        }
        return $this->render("Product/details.html.twig", [
           "product" => $product
        ]);
    }

    public function add()
    {
        $product = new Product();
        $errors = $this->handleRequest($product);
        if ($this->isPostRequest() && !count($errors)) {
            $repo = new ProductRepository();
            $repo->insert($product);
            $this->addFlash("success", "Le produit " . $product->getName() . " a été ajouté.");
            return $this->redirect("Product", "index");
        }
        if(count($errors)) {
            $this->addFlash("error", "Le produit " . $product->getName() . " n'a pas pu être ajouté.");
        }
        return $this->render("Product/add-edit.html.twig", [
            "product" => $product,
            "errors" => $errors
        ]);
    }

    public function edit() {
        $id = $this->get("id");
        $repo = new ProductRepository();
        $product = $repo->get($id);
        if(!$product) {
            throw new NotFoundException();
        }
        $errors = $this->handleRequest($product);
        if ($this->isPostRequest() && !count($errors)){
            $repo->update($product);
            $this->addFlash("success", "Le produit " . $product->getName() . " a été modifié.");
            return $this->redirect("Product", "index");
        }
        if(count($errors)) {
            $this->addFlash("error", "Le produit " . $product->getName() . " n'a pas pu être modifié.");
        }
        return $this->render("Product/add-edit.html.twig", [
            "product" => $product,
            "errors" => $errors
        ]);
    }

    public function delete() {
        $id = $this->get("id");
        $repo = new ProductRepository();
        $product = $repo->get($id);
        if(!$product){
            throw new NotFoundException();
        }
        $repo->delete($id);
        $this->addFlash("success", "Le produit " . $product->getName() . " a été supprimé.");
        return $this->redirect("product", "index");
    }
}