<?php
namespace Core\controller;


use Core\exception\NotFoundException;
use Core\exception\ValidationException;
use Core\session\FlashBag;
use Exception;
use Twig\Environment;

abstract class BaseController
{
    private $twig;

    private $flashBag;
    
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
        $this->flashBag = new FlashBag();
        $this->twig->addGlobal('flashBag', $this->flashBag);
    }

    /**
     * @return bool
     */
    protected function isPostRequest()
    {
        return $_SERVER["REQUEST_METHOD"] === "POST";
    }

    /**
     * @param $key
     * @return string|null
     */
    protected function get($key)
    {
        return isset($_GET[$key]) ? $_GET[$key] : null;
    }

    protected function render($view, $parameters = []) 
    {
        return $this->twig->render($view, $parameters);
    }

    protected function redirect($controller, $method){
        return header("Location: " . SITE_DOMAIN ."?controller=$controller&method=$method");
    }

    /**
     * @param $instance
     * @return array
     */
    protected function handleRequest($instance)
    {
        $errors = [];
        if($this->isPostRequest()){
            foreach ($_POST as $key => $value) {
                try {
                    $methodName = "set" . ucfirst($key);
                    if(method_exists($instance, $methodName)){
                        $instance->$methodName(trim($value) === "" ? null : $value);
                    }
                } catch (ValidationException $e) {
                    $errors[$key] = $e->getMessage();
                }
            }
        }
        return $errors;
    }

    protected function addFlash($type, $message)
    {
        $this->flashBag->addFlash($type, $message);
    }
}