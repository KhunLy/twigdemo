<?php


namespace Core\session;


use http\Message;

class FlashBag
{
    public function addFlash($type, $message) {
        if (!isset($_SESSION[$type])) {
            $_SESSION[$type] = [];
        }
        $_SESSION[$type][] = $message;
    }

    public function getFlashes($type) {
        if (isset($_SESSION[$type])){
            $flashes = $_SESSION[$type];
            $_SESSION[$type] = [];
        }
        else {
            $flashes = [];
        }
        return $flashes;
    }
}